#!/bin/bash
# to make this file executable run 'chmod +x run_matlab.sh'
# then to exectute it run './run_matlab.sh'

wd='demos/matlab/'
#files in the working directory
out='log.out'
err='log.err'
script='test'

cd $wd
nohup matlab -batch $script 1>$out 2>$err &
