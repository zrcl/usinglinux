#!/bin/bash
# to make this file executable run 'chmod +x run_python.sh'
# then to exectute it run './run_python.sh'

wd='demos/python/'
#files in the working directory
out='log.out'
err='log.err'
script='main.py'

cd $wd
nohup python $script 1>$out 2>$err &
