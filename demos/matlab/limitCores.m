clear all;
close all;

% set the number of threads
% should not exceed the number of threads of a single proc if you don't have the distributed toolbox
nThreads=3;
maxNumCompThreads(nThreads);

n = 10000;
a=rand(n);
b=rand(n);
c=a*b;
