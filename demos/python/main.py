"""
Limiting the number of threads with numpy
"""
import os
import time

nThreads="4"
os.environ["OMP_NUM_THREADS"] = nThreads # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = nThreads # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = nThreads # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = nThreads # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = nThreads # export NUMEXPR_NUM_THREADS=6

import numpy as np

if __name__=='__main__':
	n=5000
	a=np.random.rand(n,n)
	b=np.random.rand(n,n)

	c=a @ b # matmul(a,b)
	print('done')

	#error on purpose
	c=d
